﻿USE practica2;
-- Consultas

-- Indicar el nº de ciudades que hay en la tabla ciudades.

SELECT COUNT(*) ciudades FROM Ciudad c;

-- Indicar el nombre de las ciudades que tengan una población por encima de la población media.

SELECT * FROM Ciudad c
WHERE c.población>(SELECT AVG(c1.población) FROM Ciudad c1);

-- Indicar el nombre de las ciudades que tengan una población por debajo de la población media.

SELECT * FROM ciudad c
WHERE c.población<(SELECT AVG(c1.población) FROM ciudad c1);

-- Indicar el nombre de la ciudad con la población máxima.

SELECT * FROM ciudad c
WHERE c.población= (SELECT MAX(c1.población) FROM ciudad c1);

-- Indicar el nombre de la ciudad con la población mínima.

SELECT * FROM ciudad c
WHERE c.población= (SELECT MIN(c1.población) FROM ciudad c1);


-- Indicar el nº de ciudades que tengan una población por encima de la población media.

SELECT COUNT(*)nºciudades FROM  ciudad c
WHERE c.población>(SELECT AVG(c1.población) FROM Ciudad c1);


-- Indicar el nº de personas que viven en cada ciudad.

SELECT COUNT(*)nºpersonas, p.ciudad FROM persona p
GROUP BY p.ciudad;

-- Utilizando la tabla trabaja indicar cuantas personan trabajan en cada una de las compañías.
  
SELECT COUNT(*)nºpersonas, t.compañia FROM trabaja t 
GROUP BY t.compañia;

-- Indicar la compañía que más trabajares tiene.

SELECT t.compañia FROM trabaja t
GROUP BY t.compañia
HAVING COUNT(*) = 
                      (
   SELECT 
   MAX(C1.contador) max_contador
   FROM (
   SELECT 
   compañia, COUNT(*) contador
   FROM 
   trabaja 
GROUP BY compañia) C1
                       );

-- Indicar el salario medio de cada una de las compañías.

SELECT AVG(t.salario)salario_medio, t.compañia FROM trabaja t
GROUP BY t.compañia;

-- Listarme el nombre de las personas y la población de la ciudad donde viven.

SELECT p.nombre, c.población
    FROM ciudad c 
    JOIN persona p 
    ON c.nombre=p.ciudad;

-- Listar el nombre de las personas la calle donde viven y la población de la ciudad donde viven.

SELECT p.nombre, p.calle,c.población
    FROM 
    ciudad c 
    JOIN persona p 
    ON c.nombre = p.ciudad;

-- Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja.

SELECT 
      consulta1.nombre, 
      consulta1.ciudad, 
      c.ciudad ciudad_compañia
  FROM compañia c 
    JOIN
                      (SELECT
      consulta2.nombre,
      consulta2.ciudad,
      t.compañia 
  FROM trabaja t 
     JOIN 
     (SELECT p.nombre,ciudad FROM persona p) consulta2
 ON t.persona=consulta2.nombre) consulta1  
 ON consulta1.compañia=c.nombre;

  

 





